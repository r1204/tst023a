// RNAIR-2: Check the change of slides in customer feedbacks carouselle by clicking points below the carouselle

describe('As a user I want to be able to', function() {
    it('check the change of slides in customer feedbacks carouselle by clicking points below the carouselle', function() {
        browser.waitForAngularEnabled(false);
        // browser.get('https://rooms.ryanair.com/gb/en');
        browser.get('/', 20000);

        let sliderImgSel = 'body > rooms-root > rooms-home > rooms-aem-banner > a > picture > img';
        let sliderImgSrcExp = 'https://www.ryanair.com/content/dam/ryanair/2019/rooms/09/rooms-mkt-4034/microsite/desktop/EN_Rooms-Microsite-Desktop.jpg';
        let sliderImgSrcExp_tablet = 'https://www.ryanair.com/content/dam/ryanair/2019/rooms/09/rooms-mkt-4034/microsite/tablet/EN_Rooms-Microsite-Tablet.jpg';

        let btn1Sel = 'body > rooms-root > rooms-home > rooms-aem-banner > div > div:nth-child(1)';
        let btn2Sel = 'body > rooms-root > rooms-home > rooms-aem-banner > div > div:nth-child(2)';
        let btn3Sel = 'body > rooms-root > rooms-home > rooms-aem-banner > div > div:nth-child(3)';

        // browser.sleep(1500);
        let sliderImgEl = element(by.css(sliderImgSel));
        let btn1El = element(by.css(btn1Sel));
        let btn2El = element(by.css(btn2Sel));
        let btn3El = element(by.css(btn3Sel));

        sliderImgEl.getAttribute('src').then(result => {
            console.log(result);
            btn2El.click();
            return sliderImgEl.getAttribute('src');
        }).then(result => {
            console.log(result);
            expect(result).not.toEqual(sliderImgSrcExp);
        });

        console.log("===asyncVisualCheck===");
        browser.sleep(2000);

    });
});