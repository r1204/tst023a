exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['spec-RNAIR-002.js'],
    baseUrl: 'https://rooms.ryanair.com/gb/en/',
    allScriptsTimeout: 30000,
    onPrepare: function() {
        browser.manage().window().setSize(1366, 768);
    }
}